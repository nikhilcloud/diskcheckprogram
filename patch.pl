#!/usr/bin/Perl
# This script will add few entries required for running of Disk_Usage.pl script
# Happy Coading.......

use strict;
use warnings;

# Reducing output buffering
$|=1;

# Sub Routine to patch ssh config file
sub patch {
        my $logname = $ENV{ LOGNAME };
        my $file = "/Users/$logname/.ssh/config";

        # Modifying the values
        open(OUTPUT,">>$file") or die("File not found \n");
        print OUTPUT "StrictHostKeyChecking no\nUserKnownHostsFile=/dev/null\nCiphers aes128-ctr,aes192-ctr,aes256-ctr,arcfour256,arcfour128\nMACs hmac-sha1,hmac-ripemd160";
        close OUTPUT;
        print "Patching done. Now run Disk_Usage.pl Script\n";
};

# Calling main function
patch ();
